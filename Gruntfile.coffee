module.exports = (grunt) ->

	grunt.initConfig
	
		pkg: grunt.file.readJSON('package.json')

		paths:
			src: 'sterlinglentz/assets'
			dest: 'sterlinglentz/static/build'

		watch:
			sass:
				files: ["<%= paths.src %>/**/*.scss"]
				tasks: ['compass']
				options:
					livereload: 35740

		compass:
			dist:
				options:
					noLineComments: false
					# debugInfo: true
					sassDir: '<%= paths.src %>/sass'
					cssDir: '<%= paths.dest %>/'
					environment: 'development'
					# require: [
					# 	'susy'
					# 	'bootstrap-sass'
					# 	# 'breakpoint'
					# ]


		grunt.loadNpmTasks("grunt-contrib-watch")
		grunt.loadNpmTasks("grunt-contrib-compass")
		grunt.loadNpmTasks("grunt-notify")

		grunt.registerTask "default", [
			"compass"
			"watch"
		]