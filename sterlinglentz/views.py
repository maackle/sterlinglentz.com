from django.views import generic

import models


class WorkListView(generic.TemplateView):
	template_name = 'work_list.html'

	def get_context_data(self, **kwargs):
		ctx = super(WorkListView, self).get_context_data(**kwargs)
		ctx.update({
			'work_projects': models.WorkProject.objects.all()
		})
		return ctx


class WorkDetailView(generic.TemplateView):
	template_name = 'work_detail.html'

	def get_context_data(self, **kwargs):
		slug = kwargs['slug']
		project = models.WorkProject.objects.filter(slug=slug)[0]
		ctx = super(WorkDetailView, self).get_context_data(**kwargs)
		ctx.update({
			'project': project
		})
		return ctx
