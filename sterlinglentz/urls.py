from django.conf.urls import *  # NOQA
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from cms.sitemaps import CMSSitemap
from cms.admin.placeholderadmin import PlaceholderAdminMixin
from sorl.thumbnail.admin import AdminImageMixin

import models
import views

class WorkProjectAdmin(PlaceholderAdminMixin, AdminImageMixin, admin.ModelAdmin):
	pass
	# prepopulated_fields = {"slug": ("name",)}

admin.autodiscover()
admin.site.register(models.WorkProject, WorkProjectAdmin)


urlpatterns = i18n_patterns('',
	# url(r'^work/$', views.WorkListView.as_view(), name='work'),
	# url(r'^work/(?P<slug>[^/]+)/$', views.WorkDetailView.as_view(), name='work'),

    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^', include('cms.urls')),
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ) + staticfiles_urlpatterns() + urlpatterns  # NOQA
