HOST=$LINODE

rsync -av \
--exclude ".env" \
--exclude ".git" \
--exclude "*.pyc" \
--exclude "node_modules" \
--exclude ".sass-cache" \
--exclude "sterlinglentz/local_settings.py" \
./ $HOST:/home/michael/sites/sterlinglentz.com/

ssh $HOST <<-END
	cd /var/www/sterlinglentz.com/
	workon sterling
	
	chmod g+w . project.db
	sudo chown :www-data . project.db
	
	python manage.py collectstatic --noinput
	python manage.py migrate

	sudo -i
	initctl reload-configuration
	service nginx reload
	service sterlinglentz restart
	exit
END
