
APP=sterlinglentz
DIR=$APP/migrations

rm $DIR/*
python manage.py schemamigration $APP --initial
python manage.py migrate $APP 0001 --fake --delete-ghost-migrations
