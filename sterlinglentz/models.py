from cms.models import Page
from cms.models.fields import PlaceholderField, PageField
from cms.models.pluginmodel import CMSPlugin
from django.db import models
from paintstore.fields import ColorPickerField
from sorl.thumbnail import ImageField


class Hello(CMSPlugin):
	guest_name = models.CharField(max_length=50, default='Guest')


class WorkProject(CMSPlugin):
	
	detail_page = PageField(null=True)
	# name = models.CharField(max_length=64)
	# slug = models.SlugField(max_length=64)
	main_image = ImageField(upload_to='work', null=True)
	primary_color = ColorPickerField()
	# content = PlaceholderField('content', related_name='content')
	# roles = PlaceholderField('roles', related_name='roles')
	# images = PlaceholderField('images', related_name='images')

	def __unicode__(self):
		return self.detail_page.get_title()


class CaptionPicture(CMSPlugin):
	"""
	A Picture with a Caption
	"""

	from django.utils.translation import ugettext_lazy as _
	caption = models.CharField("Caption", max_length=255)

	image = models.ImageField(
		_("image"), upload_to=CMSPlugin.get_media_path, blank=True, null=True)

	page_link = models.ForeignKey(
		Page, verbose_name=_("page"),
		help_text=_("If present image will be clickable"), blank=True,
		null=True, limit_choices_to={'publisher_is_draft': True})

	url = models.CharField(
		_("link"), max_length=255, blank=True, null=True,
		help_text=_("If present image will be clickable."))

	description = models.TextField(_("description"), blank=True, null=True)
	
	def __unicode__(self):
		return self.caption
	
	search_fields = ('description',)

