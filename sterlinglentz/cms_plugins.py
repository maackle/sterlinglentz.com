from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin

from .models import Hello, WorkProject, CaptionPicture


class HelloPlugin(CMSPluginBase):
    model = Hello
    name = "Hello Plugin"
    render_template = "hello_plugin.html"

    def render(self, context, instance, placeholder):
       context['instance'] = instance
       return context


class WorkProjectPlugin(CMSPluginBase):
    model = WorkProject
    name = "Work Project"
    render_template = "work_project_plugin.html"

    def render(self, context, instance, placeholder):
       context['instance'] = instance
       return context


class CaptionPicturePlugin(CMSPluginBase):
   model = CaptionPicture
   name = "Picture w/ Caption"
   render_template = "caption_picture_plugin.html"
   
   def render(self, context, instance, placeholder):
       if instance.url:
           link = instance.url
       elif instance.page_link:
           link = instance.page_link.get_absolute_url()
       else:
           link = ""

       context.update({
           'object': instance,
           'placeholder': placeholder,
           'link': link
       })
       return context 

plugin_pool.register_plugin(WorkProjectPlugin)
plugin_pool.register_plugin(CaptionPicturePlugin)
plugin_pool.register_plugin(HelloPlugin)